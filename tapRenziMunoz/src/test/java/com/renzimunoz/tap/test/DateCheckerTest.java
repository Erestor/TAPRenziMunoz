package com.renzimunoz.tap.test;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.renzimunoz.tap.core.DateChecker;

public class DateCheckerTest {
	private long milliSecond = 100000;
	private Date start;
	private Date end;
	
	@Before
	public void setUp(){
		start = new Date(milliSecond);
		end = new Date(milliSecond+300);
	}
	
	// Prenotation 		+---------+
	// newPrenotation	+---------+
	@Test
	public void testIsFreeFalseCauseSameTime(){
		assertTime(0, 0, false);
	}
	
	// Prenotation 		+---------+
	// newPrenotation	  +-----+	
	@Test
	public void testIsFreeFalseCauseContainedInTime(){
		assertTime(100, -100, false);
	}
	
	// Prenotation 		  +-----+
	// newPrenotation	+---------+	
	@Test
	public void testIsFreeFalseCauseTimeContained(){
		assertTime(-100, +100, false);
	}
	
	// Prenotation 		+---------+
	// newPrenotation	   +---------+	
	@Test
	public void testIsFreeFalseCauseStartContained(){
		assertTime(+100, +100, false);
	}
	
	// Prenotation 		   +---------+
	// newPrenotation	+---------+	
	@Test
	public void testIsFreeFalseCauseEndContained(){
		assertTime(-100, -100, false);
	}
	
	// Prenotation 		+------+
	// newPrenotation	+---------+	
	@Test
	public void testIsFreeFalseCauseSameStart(){
		assertTime(0, +100, false);
	}
	 
	// Prenotation 		    +-----+
	// newPrenotation	+---------+	
	@Test
	public void testIsFreeFalseCauseSameEnd(){
		assertTime(-100, 0, false);
	}
	
	// Prenotation 		+---------+
	// newPrenotation	+-----+	
	@Test
	public void testIsFreeFalseCausesameStartEndBeforePrenotationEnd(){
		assertTime(0, -100, false);
	}	
	
	// Prenotation 		+---------+
	// newPrenotation	    +-----+	
	@Test
	public void testIsFreeFalseCausesameEndStartAfterPrenotationStart(){
		assertTime(+100, 0, false);
	}	
	
	// Prenotation 		+---------+
	// newPrenotation	    	  +-------+		
	@Test
	public void testIsFreeTrueCauseSameStart(){
		assertTime(+300,+300,true);
	}
	
	// Prenotation 				+---------+
	// newPrenotation	+-------+		
	@Test
	public void testIsFreeTrueCauseSameEnd(){
		assertTime(-300,-300,true);
	}
	
	// Prenotation 				   +---------+
	// newPrenotation	+-------+		
	@Test
	public void testIsFreeTrueCauseisBefore(){
		assertTime(-400,-400,true);
	}
	
	// Prenotation 		+---------+
	// newPrenotation			    +-------+		
	@Test
	public void testIsFreeTrueCauseisAfter(){
		assertTime(+400,+400,true);
	}
	
	@Test
	public void testIsFreeFalseCauseStartAfterEnd(){
		assertTime(+300,-300,false);
	}
	
	private void assertTime(int startPlus, int endPlus, boolean expected){
		Date nStart = new Date(start.getTime()+startPlus); 
		Date nEnd = new Date(end.getTime()+endPlus);
		boolean res = DateChecker.isFree(start,end,nStart,nEnd);
		assertEquals(expected,res);
	}
	
	@Test
	public void testJustToSeeAllGreen(){
		DateChecker dc = null;
        try {
        	Constructor<?>[] costructors = DateChecker.class.getDeclaredConstructors();
        	costructors[0].setAccessible(true);
        	dc = (DateChecker)costructors[0].newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(dc);
	}
}
