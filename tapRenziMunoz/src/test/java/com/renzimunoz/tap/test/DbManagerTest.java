package com.renzimunoz.tap.test;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import javax.persistence.Persistence;
import org.junit.Test;
import com.renzimunoz.tap.core.Database;
import com.renzimunoz.tap.core.DbManager;
import com.renzimunoz.tap.core.MySqlWrapper;

public class DbManagerTest {

	@Test
	public void testSingolaIstanza() {
		Database database = new MySqlWrapper(Persistence.createEntityManagerFactory("tapRenziMunozTEST"));
		DbManager dbManager = DbManager.getInstance();
		dbManager.setDatabase(database);
		DbManager dbManager2 = DbManager.getInstance();
		assertEquals(dbManager.getDatabase(), dbManager2.getDatabase());
		assertNotNull(dbManager);
	}
	
	@Test
	public void testDoppiaIstanza(){
		DbManager.getInstance();
        try {
        	Field instance = DbManager.class.getDeclaredField("dbManager");
        	instance.setAccessible(true);
        	instance.set(null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        DbManager db2 = DbManager.getInstance();
        assertNotNull(db2);
	}
}
