package com.renzimunoz.tap.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.renzimunoz.tap.core.Database;
import com.renzimunoz.tap.core.DbManager;
import com.renzimunoz.tap.core.Prenotation;
import com.renzimunoz.tap.core.Room;

public class RoomTest {
	private DbManager dbManager;
	private Database db;
	private Room room;
	private long milliSecond = 100000;
	private Date start;
	private Date end;
	private List<Prenotation> prenotations;
	
	@Before
	public void setUp(){
		dbManager = DbManager.getInstance();
		db = mock(Database.class);
		dbManager.setDatabase(db);
		start = new Date(milliSecond);
		end = new Date(milliSecond+300);
		prenotations = new LinkedList<>();
		when(db.getPrenotations(isA(Room.class))).thenReturn(prenotations);
	}
	
	@Test
	public void testIsFreeWhithNoPrenotationsSoTrue(){
		room = new Room("Stanza delle meraviglie");
		assertTrue(room.isFree(start,end));
	}
	
	@Test
	public void testReserveWithoutPrenotationSuccess() {
		room = new Room("Stanza delle meraviglie");
		when(db.reserve(room, start, end)).thenReturn(mock(Prenotation.class));
		assertTrue(room.reserve(start, end));
		assertEquals(1, room.getPrenotations().size());
		verify(db,times(1)).reserve(room,start,end);
	}
	
	@Test
	public void testReserveWithOnePrenotationSuccess() {
		Prenotation prenotation = mock(Prenotation.class);
		when(prenotation.getStart()).thenReturn(start);
		when(prenotation.getEnd()).thenReturn(end);
		prenotations.add(prenotation);
		room = new Room("Stanza delle meraviglie");
		Date nStart = new Date(start.getTime()+500);
		Date nEnd = new Date(end.getTime()+500);
		when(db.reserve(room, nStart, nEnd)).thenReturn(mock(Prenotation.class));
		assertTrue(room.reserve(nStart, nEnd));
		assertEquals(2, room.getPrenotations().size());
		verify(db,times(1)).reserve(room, nStart, nEnd);
	}
	
	@Test
	public void testReserveWithOnePrenotationFail() {
		Prenotation prenotation = mock(Prenotation.class);
		when(prenotation.getStart()).thenReturn(start);
		when(prenotation.getEnd()).thenReturn(end);
		prenotations.add(prenotation);
		room = new Room("Stanza delle meraviglie");
		when(db.reserve(room, start, end)).thenReturn(mock(Prenotation.class));
		assertFalse(room.reserve(start, end));
		assertEquals(1, room.getPrenotations().size());
		verify(db,times(0)).reserve(room, start, end);
	}
	
	@Test
	public void testReserveFailCauseDbProblem(){
		room = new Room("Stanza delle meraviglie");
		when(db.reserve(room, start, end)).thenReturn(null);
		assertFalse(room.reserve(start, end));
		assertEquals(0, room.getPrenotations().size());
		verify(db,times(1)).reserve(room,start,end);
	}
	
	@Test
	public void testRemovePrenotationWhithoutPrenotation(){
		room = new Room("Stanza delle meraviglie");
		Prenotation prenotation = mock(Prenotation.class);
		assertFalse(room.remove(prenotation));
		verify(db,times(0)).removePrenotation(prenotation);
	}
	
	@Test
	public void testRemovePrenotationFailWithOnePrenotationInList(){
		Prenotation prenotation = mock(Prenotation.class);
		when(prenotation.getStart()).thenReturn(start);
		when(prenotation.getEnd()).thenReturn(end);
		when(prenotation.getId()).thenReturn(1);
		prenotations.add(prenotation);
		room = new Room("Stanza delle meraviglie");
		Prenotation prenotationToRemove = mock(Prenotation.class);
		Date nStart = new Date(start.getTime()+500);
		Date nEnd = new Date(end.getTime()+500);
		when(prenotationToRemove.getStart()).thenReturn(nStart);
		when(prenotationToRemove.getEnd()).thenReturn(nEnd);
		when(prenotationToRemove.getId()).thenReturn(2);
		assertFalse(room.remove(prenotationToRemove));
		assertEquals(1, room.getPrenotations().size());
		verify(db,times(0)).removePrenotation(prenotationToRemove);
	}

	@Test
	public void testRemovePrenotationSuccessWithOnePrenotationInList(){
		Prenotation prenotation = mock(Prenotation.class);
		when(prenotation.getStart()).thenReturn(start);
		when(prenotation.getEnd()).thenReturn(end);
		when(prenotation.getId()).thenReturn(1);
		prenotations.add(prenotation);
		room = new Room("Stanza delle meraviglie");
		assertTrue(room.remove(prenotation));
		assertEquals(0, room.getPrenotations().size());
		verify(db,times(1)).removePrenotation(prenotation);
	}
}
