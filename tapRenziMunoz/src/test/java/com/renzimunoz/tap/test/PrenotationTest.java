package com.renzimunoz.tap.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.renzimunoz.tap.core.Database;
import com.renzimunoz.tap.core.DbManager;
import com.renzimunoz.tap.core.Item;
import com.renzimunoz.tap.core.Prenotation;
import com.renzimunoz.tap.core.Room;

public class PrenotationTest {
	private DbManager dbManager;
	private Database db;
	private Room room;
	private Date start;
	private Date end;
	private Prenotation prenotation;
	private List<Item> items;

	@Before
	public void setUp() {
		items = new LinkedList<>();
		dbManager = DbManager.getInstance();
		db = mock(Database.class);
		when(db.getPrenotationItems(isA(Prenotation.class))).thenReturn(items);
		dbManager.setDatabase(db);
		room = new Room("Stanza delle meraviglie");
		start = new Date(System.currentTimeMillis());
		end = new Date(System.currentTimeMillis()+300);
		prenotation = new Prenotation(room,start,end);
	}
	
	@Test
	public void testAddItemInEmptyListOfPrenotationItems(){
		Item item = spy(new Item("category","model"));
		when(item.getId()).thenReturn(1);
		when(db.addItem(prenotation, item)).thenReturn(true);
		assertEquals(true,prenotation.addItem(item));
		assertEquals(1,prenotation.getItems().size());
		assertEquals("category",prenotation.getItems().get(0).getCategory());
		assertEquals("model",prenotation.getItems().get(0).getModel());
		assertEquals(1,prenotation.getItems().get(0).getId());	
		verify(db,times(1)).addItem(isA(Prenotation.class),isA(Item.class));
	}
	

	@Test
	public void testAddItemAlreadyReserved(){
		Item item = new Item("category","model");
		when(db.addItem(prenotation, item)).thenReturn(true);
		Room anotherRoom = new Room("Stanza delle meno meraviglie");
		Prenotation anotherPrenotation = new Prenotation(anotherRoom,start,end);
		when(db.addItem(anotherPrenotation, item)).thenReturn(false);
		assertEquals(false,anotherPrenotation.addItem(item));
		assertEquals(0,prenotation.getItems().size());
		verify(db,times(1)).addItem(isA(Prenotation.class),isA(Item.class));
	}
	
	@Test
	public void testDeleteItemFromAnEmptyListOfPrenotationItems(){
		Item item = new Item("category","model");
		when(db.deleteItem(prenotation, item)).thenReturn(false);
		assertEquals(false,prenotation.deleteItem(item));
		assertEquals(0,prenotation.getItems().size());
		verify(db,times(1)).deleteItem(isA(Prenotation.class), isA(Item.class));
	}
	
	@Test
	public void testDeleteItemFromAPrenotationWithItems(){
		Item item = new Item("category","model");
		when(db.addItem(prenotation, item)).thenReturn(true);
		assertEquals(true, prenotation.addItem(item));
		assertEquals(1,prenotation.getItems().size());
		verify(db,times(1)).addItem(isA(Prenotation.class),isA(Item.class));
		when(db.deleteItem(prenotation, item)).thenReturn(true);
		assertEquals(true,prenotation.deleteItem(item));
		assertEquals(0,prenotation.getItems().size());
		verify(db,times(1)).deleteItem(isA(Prenotation.class), isA(Item.class));
	}
	
}
