package com.renzimunoz.tap.integration;

import com.renzimunoz.tap.MySqlWrapperAbstract;

public class MySqlWrapperIT extends MySqlWrapperAbstract{
	@Override
	public String persistenceConfig() {
		return "tapRenziMunozINTEGRATION";
	}
}
