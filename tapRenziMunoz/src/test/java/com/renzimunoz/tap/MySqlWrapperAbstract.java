package com.renzimunoz.tap;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.renzimunoz.tap.core.Database;
import com.renzimunoz.tap.core.DbManager;
import com.renzimunoz.tap.core.Item;
import com.renzimunoz.tap.core.MySqlWrapper;
import com.renzimunoz.tap.core.Prenotation;
import com.renzimunoz.tap.core.Room;

import org.jinq.orm.stream.JinqStream.Join;
import org.jinq.orm.stream.JinqStream.Where;

public abstract class MySqlWrapperAbstract {
	private DbManager dbManager;
	private MySqlWrapper db;
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private long milliSecond = 100000;
	private Date start;
	private Date end;

	@Before
	public void setUp() {
		dbManager = DbManager.getInstance();
		entityManagerFactory = Persistence.createEntityManagerFactory(persistenceConfig());
		db = new MySqlWrapper(entityManagerFactory);
		dbManager.setDatabase(db);
		connection();
		start = new Date(milliSecond);
		end = new Date(milliSecond + 1000);
	}

	public abstract String persistenceConfig();

	@After()
	public void tearDown() {
		closeConnection();
	}
	
	@Test
	public void testGetRoomsWithoutRooms(){
		assertEquals(0, db.getRooms().size());
	}
	
	@Test
	public void testGetRoomsWithOneRooms(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		List<Room> rooms = db.getRooms();
		assertEquals(1, rooms.size());
		assertEquals("Stanza delle meraviglie", rooms.get(0).getName());
	}
	
	@Test
	public void testGetItemsWithoutItems(){
		assertEquals(0, db.getRooms().size());
	}
	
	@Test
	public void testGetItemsWithOneItem(){
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		List<Item> items = db.getItems();
		assertEquals(1, items.size());
		assertEquals("microfono", items.get(0).getCategory());
	}
	
	@Test
	public void testGetPrenotationsWhithoutPrenotations() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		assertEquals(0, db.getPrenotations(room).size());
	}

	@Test
	public void testGetPrenotationsWhithOnePrenotation() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		List<Prenotation> getPrenotations = db.getPrenotations(room);
		assertEquals(1, getPrenotations.size());
		assertEquals(1, getPrenotations.get(0).getId());
		assertEquals(1, getPrenotations.get(0).getRoom().getId());
		assertEquals(start.getTime(), getPrenotations.get(0).getStart().getTime());
		assertEquals(end.getTime(), getPrenotations.get(0).getEnd().getTime());
		assertEquals("Stanza delle meraviglie", getPrenotations.get(0).getRoom().getName());
	}

	@Test
	public void testLambdaGetPrenotationsSuccess() throws Exception {
		assertLambdaGetPrenotations(true, 1);
	}

	@Test
	public void testLambdaGetPrenotationsFail() throws Exception {
		assertLambdaGetPrenotations(false, 0);
	}
	
	@Test
	public void testgetPrenotableItemsWithutItems(){
		assertEquals(0, db.getPrenotableItems(start, end).size());
	}
	
	@Test
	public void testgetPrenotableItemsWithOneItemNotPrenoted(){
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		List<Item> items = db.getPrenotableItems(start, end);
		assertEquals(1, items.size());
		assertEquals(1, items.get(0).getId());
	}
	
	@Test
	public void testgetPrenotableItemsWithOneItemPrenoted(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		prenotation.getItems().add(item);
		insertPrenotation(prenotation);
		assertEquals(0, db.getPrenotableItems(start, end).size());
	}
	
	@Test
	public void testgetPrenotableItemsWithOneItemPrenotedAndOtherItemNotPrenoted(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		insertItem(new Item("Cavo", "Jack"));
		Prenotation prenotation = new Prenotation(room, start, end);
		prenotation.getItems().add(item);
		insertPrenotation(prenotation);		
		List<Item> items = db.getPrenotableItems(start, end);
		assertEquals(1, items.size());
		assertEquals(2, items.get(0).getId());
		
	}
	
	@Test
	public void testIsPrenotableItemTrue(){
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		assertTrue(db.isPrenotableItem(item, start, end));
	}
	
	@Test
	public void testIsPrenotableItemFalse(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		db.addItem(prenotation,item);
		prenotation.getItems().add(item);
		assertFalse(db.isPrenotableItem(item, start, end));
	}
	
	@Test
	public void testIsPrenotableItemTrueWithItemPrenotatedAtDifferentTime(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		db.addItem(prenotation,item);
		prenotation.getItems().add(item);
		Date nStart = new Date(start.getTime()+10000);
		Date nEnd = new Date(end.getTime()+10000);
		assertTrue(db.isPrenotableItem(item, nStart, nEnd));
	}
	
	@Test
	public void testReserveSuccessReseved() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = db.reserve(room, start, end);
		List<Room> rooms = db.getRooms();
		List<Prenotation> getPrenotation = db.getPrenotations(rooms.get(0));
		assertNotNull(prenotation);
		assertEquals(1, getPrenotation.get(0).getId());
		assertEquals(start.getTime(), getPrenotation.get(0).getStart().getTime());
		assertEquals(end.getTime(), getPrenotation.get(0).getEnd().getTime());
		assertEquals(room.getName(), getPrenotation.get(0).getRoom().getName());
	}

	@Test
	public void testReserveWhithEndBeforeStart() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		assertNull(db.reserve(room, end, start));
	}

	@Test
	public void testRemovePrenotation() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		db.removePrenotation(prenotation);
		List<Room> rooms = db.getRooms();
		assertEquals(0, db.getPrenotations(rooms.get(0)).size());
		db.reserve(room, start, end);
	}

	@Test
	public void testAddItemsSuccessAdding() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertTrue(db.addItem(prenotation, item));
		List<Item> getItems = db.getPrenotationItems(prenotation);
		assertEquals(1, getItems.size());
		assertEquals(1, getItems.get(0).getId());
		assertEquals("microfono", getItems.get(0).getCategory());
		assertEquals("SM58", getItems.get(0).getModel());
		assertEquals(1, getItems.get(0).getPrenotations().size());
		assertEquals(1, getItems.get(0).getPrenotations().get(0).getId());
	}

	@Test
	public void testAddItemNull() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertFalse(db.addItem(prenotation, null));
		assertEquals(0, db.getPrenotationItems(prenotation).size());
	}

	@Test
	public void testLambdaGetItemsSuccess() throws Exception {
		assertLambdaGetItems(true, 1);
	}

	@Test
	public void testLambdaGetItemsFail() throws Exception {
		assertLambdaGetItems(false, 0);
	}
	
	@Test
	public void testGetItemsPrenotationsWhithNoItem(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertEquals(0, db.getPrenotationItems(prenotation).size());
	}
	
	@Test
	public void testGetPrenotationItemsWhithOneItem(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		Prenotation prenotation = new Prenotation(room, start, end);
		prenotation.getItems().add(item);
		item.getPrenotations().add(prenotation);
		insertItem(item);
		insertPrenotation(prenotation);
		List<Item> items = db.getPrenotationItems(prenotation);
		assertEquals(1, items.size());
		assertEquals("microfono", items.get(0).getCategory());
		assertEquals(start, items.get(0).getPrenotations().get(0).getStart());		
	}
	
	@Test
	public void testLambdaJinqStreamFail() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		List<Item> items = new LinkedList<>();
		items.add(new Item("microfono", "SM58"));
		insertItem(items.get(0));
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		Join<Prenotation, Item> lambda = db.lambdaJinqStream();
		assertEquals(0, lambda.join(prenotation).count());
	}

	private void assertLambdaGetPrenotations(boolean expected, int idRoom) throws Exception {
		Where<Prenotation, Exception> a = db.lambdaGetPrenotations(idRoom);
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertEquals(expected, a.where(prenotation));
	}

	private void assertLambdaGetItems(boolean expected, int idPrenotation) throws Exception {
		Where<Prenotation, Exception> lambda = db.lambdaGetItems(idPrenotation);
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertEquals(expected, lambda.where(prenotation));
	}

	@Test
	public void testGetItemPrenotationsWhereItemIsNotReserved(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertEquals(0,db.getItemPrenotations(item).size());
	}
	
	@Test
	public void testGetItemPrenotationsWhereItemsIsReserved(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		List<Item> newItemsList = prenotation.getItems();
		newItemsList.add(item);
		item.getPrenotations().add(prenotation);
		insertItem(item);
		assertEquals(1,db.getItemPrenotations(item).size());
		assertEquals(end.getTime(),db.getItemPrenotations(item).get(0).getEnd().getTime());
	}
	
	@Test
	public void testLambdaGetItemsPrenotationsSucces() throws Exception {
		assertLambdaGetItemsPrenotation(true, 1);
	}
	
	@Test
	public void testLambdaGetItemsPrenotationsFail() throws Exception {
		assertLambdaGetItemsPrenotation(false, 0);
	}
	
	@Test
	public void testlambdaJinqStreamGetItemsPrenotationsFail() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		List<Item> items = new LinkedList<>();
		items.add(new Item("microfono", "SM58"));
		insertItem(items.get(0));
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		Join<Item, Prenotation> lambda = db.lambdaJinqStreamGetItemsPrenotations();
		assertEquals(0, lambda.join(items.get(0)).count());
	}

	
	private void assertLambdaGetItemsPrenotation(boolean expected, int itemId) throws Exception {
		Where<Item, Exception> lambda = db.lambdaGetItemsPrenotations(itemId);
		Item item = new Item("microfono","SM58");
		insertItem(item);
		assertEquals(expected, lambda.where(item));
	}
	
	@Test
	public void testDeleteNullItem() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		Item item = null;
		assertEquals(false, db.deleteItem(prenotation, item));
	}

	@Test
	public void testDeleteItemItemNotReserved() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		assertEquals(false, db.deleteItem(prenotation, item));
	}

	@Test
	public void testDeleteItemReservedInAPrenotation() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		assertTrue(db.addItem(prenotation, item));
		assertEquals(true, db.deleteItem(prenotation, item));
		List<Prenotation> prenotations = db.getRooms().get(0).getPrenotations();
		List<Item> getNewPrenotationsList = db.getPrenotationItems(prenotations.get(0));
		assertEquals(0,getNewPrenotationsList.size());
		List<Prenotation> getNewItemsList = db.getItemPrenotations(item);
		assertEquals(0, getNewItemsList.size());
		assertEquals(0, db.getPrenotationItems(prenotation).size());
		db.addItem(prenotation, item);
	}
	
	@Test
	public void testDeleteItemReservedInAPrenotationWhithNoCallToDbDotAddItems() {
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		Prenotation prenotation = new Prenotation(room, start, end);
		prenotation.getItems().add(item);
		item.getPrenotations().add(prenotation);
		insertItem(item);
		insertPrenotation(prenotation);
		assertEquals(true, db.deleteItem(prenotation, item));
		List<Item> getNewPrenotationsList = db.getPrenotationItems(prenotation);
		assertEquals(0,getNewPrenotationsList.size());
		List<Prenotation> getNewItemsList = db.getItemPrenotations(item);
		assertEquals(0, getNewItemsList.size());
		assertEquals(0, db.getPrenotationItems(prenotation).size());
	}
	
	@Test
	public void testCorrectCallAtEmptyCostructor(){
		Room room = new Room("Stanza delle meraviglie");
		insertRoom(room);
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Prenotation prenotation = new Prenotation(room, start, end);
		insertPrenotation(prenotation);
		Date nStart = new Date(start.getTime()+2000);
		Date nEnd = new Date(end.getTime()+2000);
		List<Room> rooms = db.getRooms();
		List<Prenotation> prenotations = db.getPrenotations(room);
		assertTrue(rooms.get(0).reserve(nStart, nEnd));
		assertTrue(prenotations.get(0).addItem(item));
		List<Item> items = db.getItems();
		assertEquals(1,items.get(0).getPrenotations().size());
		assertTrue(prenotations.get(0).deleteItem(item));
		items = db.getItems();
		assertEquals(0,items.get(0).getPrenotations().size());
	}
	
	
	@Test
	public void testCorrectCallAtEmptyCostructorForItem(){
		Item item = new Item("microfono", "SM58");
		insertItem(item);
		Database dbItem = null;
        try {
        	Field dbField = Item.class.getDeclaredField("db");
        	dbField.setAccessible(true);
        	dbItem = (Database)dbField.get(db.getItems().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(dbItem);
	}
	
	private void insertItem(Item item) {
		entityManager.getTransaction().begin();
		entityManager.persist(item);
		entityManager.getTransaction().commit();
	}

	private void insertPrenotation(Prenotation prenotation) {
		entityManager.getTransaction().begin();
		entityManager.persist(prenotation);
		entityManager.getTransaction().commit();
	}

	private void insertRoom(Room room) {
		entityManager.getTransaction().begin();
		entityManager.persist(room);
		entityManager.getTransaction().commit();
	}

	private void closeConnection() {
		entityManager.close();
		entityManagerFactory.close();
	}

	private void connection() {
		entityManager = entityManagerFactory.createEntityManager();
	}
}
