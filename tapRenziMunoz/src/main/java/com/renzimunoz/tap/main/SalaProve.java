package com.renzimunoz.tap.main;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.renzimunoz.tap.core.Database;
import com.renzimunoz.tap.core.DbManager;
import com.renzimunoz.tap.core.Item;
import com.renzimunoz.tap.core.MySqlWrapper;
import com.renzimunoz.tap.core.Prenotation;
import com.renzimunoz.tap.core.Room;


public class SalaProve {
	private static final Logger LOGGER = LogManager.getLogger("Sala Prove");
	private static final String PERSISTENCE_NAME = "tapRenziMunoz";
	private static final String CREATE = "docker run --detach --name=tapRenziMunozDB -p 3306:3306 -e MYSQL_ROOT_HOST=172.17.0.1 -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=tapRenziMunozDB -d mysql/mysql-server:5.7";
	private static final String START = "docker start tapRenziMunozDB";
	private static final String STOP = "docker stop  tapRenziMunozDB";
	private static final String REMOVE = "docker rm  tapRenziMunozDB";
	private static final long HALF_HOUR = 1000*60*30;
	private EntityManagerFactory entityManagerFactory;
	private Database database;
	private DbManager dbManager;
	private EntityManager entityManager;
	private Date startDate;
	private Date endDate;
	
	public static void main(String[] args){
		SalaProve salaProve = new SalaProve();
		salaProve.connect();
		salaProve.run();
	}

	private void run() {
		startDate = new Date(System.currentTimeMillis());
		endDate = new Date(System.currentTimeMillis()+(HALF_HOUR*4));
		if(isFirstRun()){
			firstExecutionWithEmptyDatabase();
		}else{
			ExecutionWithNotEmptyDatabase();
		}	
	}

	private void firstExecutionWithEmptyDatabase() {
		List<Room> rooms = database.getRooms(); 
		LOGGER.info("  Nella Struttura sono presenti "+rooms.size()+" stanze:");
		printRoomsInfo(rooms);
		List<Item> items = database.getItems();
		LOGGER.info("  Nella Struttura sono presenti "+items.size()+" Attrezzature:");
		printItemsInfo(items);
		Date start = new Date(startDate.getTime());
		Date end = new Date(endDate.getTime());
		prenota(rooms.get(0),start,end);
		start = new Date(startDate.getTime()+(HALF_HOUR*4));
		end = new Date(endDate.getTime()+(HALF_HOUR*4));
		prenota(rooms.get(0),start,end);
		prenota(rooms.get(1),start,end);
		List<Prenotation> prenotations1 = rooms.get(0).getPrenotations();
		Prenotation p = prenotations1.get(0);
		List<Item> prenotableItems = database.getPrenotableItems(p.getStart(),p.getEnd());
		LOGGER.info("  Gli oggetti prenotabili dalle ore "+p.getStart()+" alle ore "+p.getEnd()+" sono:");
		printItemsInfo(prenotableItems);
		itemPrenotation(p,prenotableItems.get(0));
		prenotableItems = database.getPrenotableItems(p.getStart(),p.getEnd());
		LOGGER.info("  Gli oggetti prenotabili dalle ore "+p.getStart()+" alle ore "+p.getEnd()+" sono:");
		printItemsInfo(prenotableItems);		close();
	}

	private void ExecutionWithNotEmptyDatabase() {
		List<Room> rooms = database.getRooms();
		LOGGER.info("  Nella Struttura sono presenti "+rooms.size()+" stanze:");
		printRoomsInfo(rooms);
		List<Item> items = database.getItems();
		LOGGER.info("  Nella Struttura sono presenti "+items.size()+" Attrezzature:");
		printItemsInfo(items);
		for(Room room:rooms){
			LOGGER.info("  Le prenotazioni presenti nella stanza "+room.getName()+" sono:");
			printPrenotationInfo(room.getPrenotations());
		}
		Prenotation p = rooms.get(0).getPrenotations().get(0);
		List<Item> prenotableItems = database.getPrenotableItems(p.getStart(),p.getEnd());
		LOGGER.info("  Gli oggetti prenotabili dalle ore "+p.getStart()+" alle ore "+p.getEnd()+" sono:");
		printItemsInfo(prenotableItems);
		p = rooms.get(0).getPrenotations().get(0);
		removeItem(p,0);
		p = rooms.get(1).getPrenotations().get(0);
		prenotableItems = database.getPrenotableItems(p.getStart(),p.getEnd());
		LOGGER.info("  Gli oggetti prenotabili dalle ore "+p.getStart()+" alle ore "+p.getEnd()+" sono:");
		printItemsInfo(prenotableItems);
		itemPrenotation(p,prenotableItems.get(0));
		removePrenotation(p);
		p = rooms.get(0).getPrenotations().get(0);
		prenotableItems = database.getPrenotableItems(p.getStart(),p.getEnd());
		LOGGER.info("  Gli oggetti prenotabili dalle ore "+p.getStart()+" alle ore "+p.getEnd()+" sono:");
		printItemsInfo(prenotableItems);
		close();
		remove();
	}

	private void prenota(Room room, Date start, Date end) {
		if(room.reserve(start, end)){
			LOGGER.info("  Effettuata prenotazione nella stanza "+room.getName()+" dalle ore "+start+" alle ore "+end);
		}else{
			LOGGER.error("  Impossibile effettuare la prenotazione.");
		}
	}
	
	private void itemPrenotation(Prenotation prenotation, Item item) {
		if(prenotation.addItem(item)){
			LOGGER.info("  Effettuata prenotazione nella stanza "+prenotation.getRoom().getName()+" dalle ore "+prenotation.getStart()+" alle ore "+prenotation.getEnd()+" di un "+item.getCategory()+" modello "+item.getModel()+"");
		}else{
			LOGGER.error("  Impossibile prenotare l'attrezzatura.");
		}
	}
	
	private void removeItem(Prenotation p, int i) {
		if(p.deleteItem(p.getItems().get(i))){
			LOGGER.info("  Rimossa attrezzatura dalla prenotazione "+p.getId()+" nella stanza "+p.getRoom().getName()+".");
		}else{
			LOGGER.error("  Impossibile rimuovere l'attrezzatura.");
		}
	}
	
	private void removePrenotation(Prenotation p) {
		if(p.getRoom().remove(p)){
			LOGGER.info("  Rimossa prenotazione dalla stanza "+p.getRoom());
		}else{
			LOGGER.error("  Impossibile rimuovere la prenotazione.");
		}
	}
	
	private void printRoomsInfo(List<Room> rooms){
		for(Room room:rooms){
			LOGGER.info(  "  "+room.getId()+") "+room.getName()+" con "+room.getPrenotations().size()+" prenotazioni.");
		}
	}
	
	private void printItemsInfo(List<Item> items){
		for(Item item:items){
			LOGGER.info(  "  "+item.getId()+") Categoria: "+item.getCategory()+", Modello: "+item.getModel()+", con "+item.getPrenotations().size()+" prenotazioni.");
		}
	}
	
	private void printPrenotationInfo(List<Prenotation> prenotations) {
		for(Prenotation prenotation:prenotations){
			LOGGER.info(  "  "+prenotation.getId()+") inizio: "+prenotation.getStart()+", fine: "+prenotation.getEnd()+", con "+prenotation.getItems().size()+" Attrezzature prenotate.");
		}
	}
	
	private boolean isFirstRun() {
		if(database.getRooms().isEmpty()){
			LOGGER.info("  -------------------------------------------------------");
			LOGGER.info("  Inserimento stanze ed attrezzature per simulare database gia' popolato.");
			insertRooms();
			insertItems();
			LOGGER.info("  Fine setUp.");
			LOGGER.info("  -------------------------------------------------------");
			return true;
		}
		return false;
	}
	
	private void insertRooms(){
		Room firstRoom = new Room("Piano terra");
		Room secondRoom = new Room("Primo piano");
		entityManager.getTransaction().begin();
		entityManager.persist(firstRoom);
		LOGGER.info("  Inserita stanza con nome 'Piano terra'.");
		entityManager.persist(secondRoom);
		entityManager.getTransaction().commit();
		LOGGER.info("  Inserita stanza con nome 'Primo piano'.");
	}
	
	private void insertItems(){
		Item[] items = {new Item("Microfono","SM58"),
						new Item("Cavo","XLR"),
						new Item("Amplificatore","Orange-Rocker30"),
						new Item("Asta","Proel-Pro200"),
						new Item("Mixer","AllenHeat-ZED"),
						new Item("Piatti","UFIP-Natural"),};
		entityManager.getTransaction().begin();
		for(Item item:items){
			LOGGER.info("  Inserita attrezzatura, categoria: '"+item.getCategory()+"', modello: '"+item.getModel()+"'.");
			entityManager.persist(item);
		}
		entityManager.getTransaction().commit();
	}
	
	private void connect(){		
		try {
			Runtime rt = Runtime.getRuntime();
			Process proc = rt.exec(START);
            int exitVal = proc.waitFor();
            if(exitVal == 0){
            	LOGGER.info("Avvio database tapRenziMunozDB: attendere.");
    			Thread.sleep(20000);
            }else{
            	LOGGER.info("Database non presente, si impostera' un container dal nome 'tapRenziMunozDB': attendere.");
				Runtime.getRuntime().exec(CREATE);
				Thread.sleep(120000);
            }
		} catch (InterruptedException | IOException e) {
			LOGGER.error(e.getMessage());
		}
		entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_NAME);
		database = new MySqlWrapper(entityManagerFactory);
		dbManager = DbManager.getInstance();
		dbManager.setDatabase(database);
		entityManager = entityManagerFactory.createEntityManager();
		LOGGER.info("Creata connessione con il database.");
	}
	
	private void close() {
		try {
			LOGGER.info("Chiusura database: attendere.");
			Runtime.getRuntime().exec(STOP);
			Thread.sleep(10000);
			LOGGER.info("Database chiuso correttamente.");
		} catch (IOException | InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}

	private void remove() {
		try {
			LOGGER.info("Rimozione container tapRenziMunozDB: attendere.");
			Runtime.getRuntime().exec(REMOVE);
			Thread.sleep(20000);
			LOGGER.info("Container tapRenziMunozDB rimosso correttamente.");
		} catch (IOException | InterruptedException e) {
			LOGGER.error(e.getMessage());
		}
	}
}
