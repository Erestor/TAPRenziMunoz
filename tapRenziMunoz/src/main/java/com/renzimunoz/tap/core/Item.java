package com.renzimunoz.tap.core;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table
public class Item implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Column
	private String category;
	
	@Column
	private String model;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToMany(cascade = CascadeType.ALL,mappedBy = "items")
	private List<Prenotation> prenotations;
	
	private transient Database db;
	
	protected Item(){
		this.db = DbManager.getInstance().getDatabase();

	}
	
	public Item(String category, String model) {
		this.category = category;
		this.model = model;
		this.db = DbManager.getInstance().getDatabase();
		this.prenotations = this.db.getItemPrenotations(this);
	}
	
	public String getModel() {
		return model;
	}
	
	public String getCategory() {
		return category;
	}
	
	public int getId() {
		return id;
	}
	
	public List<Prenotation> getPrenotations() {
		return prenotations;
	}
}
