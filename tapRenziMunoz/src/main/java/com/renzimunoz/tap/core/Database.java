package com.renzimunoz.tap.core;

import java.util.Date;
import java.util.List;

public interface Database {
	
	List<Room> getRooms();
	
	List<Item> getItems();
	
	List<Item> getPrenotableItems(Date start, Date end);
	
	boolean isPrenotableItem(Item item, Date start, Date end);
	
	List<Prenotation> getPrenotations(Room room);

	Prenotation reserve(Room room, Date start, Date end);

	void removePrenotation(Prenotation prenotation);

	List<Item> getPrenotationItems(Prenotation prenotation);

	boolean addItem(Prenotation prenotation, Item itemToAdd);

	boolean deleteItem(Prenotation prenotation, Item itemToDelete);

	List<Prenotation> getItemPrenotations(Item item);
}
