package com.renzimunoz.tap.core;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jinq.jpa.JinqJPAStreamProvider;
import org.jinq.orm.stream.JinqStream;
import org.jinq.orm.stream.JinqStream.Join;
import org.jinq.orm.stream.JinqStream.Where;
import org.jinq.tuples.Pair;

public class MySqlWrapper implements Database{
	private static final Logger LOGGER = LogManager.getLogger("MySqlWrapper");
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private JinqJPAStreamProvider streams;

	public MySqlWrapper(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		connection();
	}
	
	@Override
	public List<Room> getRooms() {
		return streams.streamAll(entityManager, Room.class).toList();
	}
	
	@Override
	public List<Item> getItems(){
		return streams.streamAll(entityManager, Item.class).toList();
	}
	
	@Override
	public List<Item> getPrenotableItems(Date start, Date end){
		List<Item> allItems =  streams.streamAll(entityManager, Item.class).toList();
		List<Item> validItems = new LinkedList<>();
		for(Item item:allItems){			
			if(isPrenotableItem(item,start,end)){
				validItems.add(item);
			}
		}
		return validItems;
	}
	
	@Override
	public boolean isPrenotableItem(Item item, Date start, Date end){
		for(Prenotation prenotation:item.getPrenotations()){
			if(!DateChecker.isFree(prenotation.getStart(), prenotation.getEnd(), start, end)){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public List<Prenotation> getPrenotations(Room room) {
		return streams.streamAll(entityManager, Prenotation.class).where(lambdaGetPrenotations(room.getId())).toList();
	}
	
	@Override
	public Prenotation reserve(Room room, Date start, Date end) {
		if(end.after(start)){
			Prenotation prenotation = new Prenotation(room,start,end);
			entityManager.getTransaction().begin();
			entityManager.persist(prenotation);
			entityManager.getTransaction().commit();
			return prenotation;
		}		
		LOGGER.error("La fine della prenotazione non può essere prima del suo inizio.");
		return null;
	}

	@Override
	public void removePrenotation(Prenotation prenotation) {
		entityManager.getTransaction().begin();
		entityManager.remove(entityManager.merge(prenotation));
		entityManager.getTransaction().commit();
	}

	@Override
	public boolean addItem(Prenotation prenotation, Item item) {
		if(item != null){
			prenotation.getItems().add(item);
			item.getPrenotations().add(prenotation);
			entityManager.getTransaction().begin();
			entityManager.merge(item);
			entityManager.getTransaction().commit();
			return true;
		}
		return false;
	}

	@Override
	public List<Item> getPrenotationItems(Prenotation prenotation) {
		JinqStream<Prenotation> prenotations = streams.streamAll(entityManager, Prenotation.class).where(lambdaGetItems(prenotation.getId()));
		List<Pair<Prenotation,Item>> reservedItem = prenotations.join(lambdaJinqStream()).toList();
		List<Item> res = new LinkedList<>();
		reservedItem.forEach(c -> res.add(c.getTwo()));
		return res;
	}
	
	@Override
	public boolean deleteItem(Prenotation prenotation, Item item) {
		if(item!=null && item.getPrenotations().contains(prenotation)){
			prenotation.getItems().remove(item);
			item.getPrenotations().remove(prenotation);
			entityManager.getTransaction().begin();
			entityManager.merge(prenotation);
			entityManager.merge(item);
			entityManager.getTransaction().commit();
			return true;
		}
		return false;
	}

	@Override
	public List<Prenotation> getItemPrenotations(Item item) {
		JinqStream<Item> itemsReserved = streams.streamAll(entityManager, Item.class).where(lambdaGetItemsPrenotations(item.getId()));
		List<Pair<Item,Prenotation>> prenotations = itemsReserved.join(lambdaJinqStreamGetItemsPrenotations()).toList();
		List<Prenotation> joinedPrenotations = new LinkedList<>();
		prenotations.forEach(p -> joinedPrenotations.add(p.getTwo()));
		return joinedPrenotations;
	}

	public Join<Item, Prenotation> lambdaJinqStreamGetItemsPrenotations() {
		return p -> JinqStream.from(p.getPrenotations());
	}

	
	public Where<Item,Exception> lambdaGetItemsPrenotations(int itemId) {
		return (i -> i.getId() == itemId);
	}

	public Join<Prenotation,Item> lambdaJinqStream() {
		return (i -> JinqStream.from(i.getItems()));
	}

	public Where<Prenotation,Exception> lambdaGetItems(int idPrenotation) {
		return (p -> p.getId() == idPrenotation);
	}
	
	public Where<Prenotation,Exception> lambdaGetPrenotations(int idRoom) {
		return (r -> r.getRoom().getId() == idRoom);
	}
	
	private void connection(){
		entityManager = entityManagerFactory.createEntityManager();
		streams = new JinqJPAStreamProvider(entityManagerFactory);
	}
}
