package com.renzimunoz.tap.core;

import java.util.Date;

public class DateChecker {
	
	private DateChecker(){}
	
	public static boolean isFree(Date start, Date end, Date nStart, Date nEnd) {
		return (start.getTime() >= nEnd.getTime()||
				end.getTime() <= nStart.getTime())&&
				nEnd.after(nStart);
	}
	

}
