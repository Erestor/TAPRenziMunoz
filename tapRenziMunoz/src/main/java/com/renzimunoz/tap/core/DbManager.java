package com.renzimunoz.tap.core;

public class DbManager {
	private static DbManager dbManager = null;
	private Database db;
	
	private DbManager() {}

	public static DbManager getInstance(){
	    if (dbManager == null){
	    	dbManager = new DbManager();
	    }
	    return dbManager; 
	}
	
	public void setDatabase(Database db){
		this.db = db;
	}
	
	public Database getDatabase(){
		return db;
	}
}
