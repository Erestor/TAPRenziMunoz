package com.renzimunoz.tap.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table
public class Prenotation implements Serializable{
	private static final long serialVersionUID = 1L;
	private transient Database db;
	
	@ManyToOne
	private Room room;
	
	@Column
	private Date start;
	
	@Column
	private Date end;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToMany
	private List<Item> items;
	
	protected Prenotation(){
		this.db = DbManager.getInstance().getDatabase();
	}
	
	public Prenotation(Room room, Date start, Date end) {
		this.db = DbManager.getInstance().getDatabase();
		this.room = room;
		this.start = start;
		this.end = end;
		items = db.getPrenotationItems(this);
	}

	public boolean addItem(Item itemToAdd) {
		if (db.addItem(this, itemToAdd)) {
			items.add(itemToAdd);
			return true;
		}
		return false;
	}

	public boolean deleteItem(Item itemToDelete) {
		if(db.deleteItem(this, itemToDelete)){
			items.remove(itemToDelete);
			return true;
		}
		return false;
	}	
	
	public Date getStart() {
		return start;
	}	
	
	public Date getEnd() {
		return end;
	}	
	
	public int getId() {
		return id;
	}	
	
	public List<Item> getItems() {
		return items;
	}	
	
	public Room getRoom() {
		return room;
	}
}
