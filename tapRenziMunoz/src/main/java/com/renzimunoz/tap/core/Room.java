package com.renzimunoz.tap.core;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table
public class Room implements Serializable{
	private static final long serialVersionUID = 1L;
	private transient Database db;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;	
	
	@Column
	private String name;	
	
	@OneToMany(mappedBy = "room")
	private List<Prenotation> prenotations;

	public Room(String name) {
		this.db = DbManager.getInstance().getDatabase();
		this.name = name;
		this.prenotations = db.getPrenotations(this);
	}
	
	protected Room(){
		this.db = DbManager.getInstance().getDatabase();
	}
	
	public boolean isFree(Date start, Date end) {
		for(Prenotation prenotation : prenotations){
			if(!DateChecker.isFree(prenotation.getStart(), prenotation.getEnd(), start, end)){
				return false;
			}				
		}
		return true;
	}

	public boolean reserve(Date start, Date end) {
		if(isFree(start,end)){	
			Prenotation prenotation = db.reserve(this, start, end);
			if(prenotation != null){
				prenotations.add(prenotation);
				return true;
			}
		}	
		return false;		
	}

	public boolean remove(Prenotation prenotationToRemove) {
		for(Prenotation prenotation : prenotations){
			if(prenotation.getId() == prenotationToRemove.getId()){
				db.removePrenotation(prenotationToRemove);
				prenotations.remove(prenotation);
				return true;
			}
		}
		return false;
	}
	
	public List<Prenotation> getPrenotations() {
		return prenotations;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
